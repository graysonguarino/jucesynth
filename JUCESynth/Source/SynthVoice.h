/*
  ==============================================================================

    SynthVoice.h
    Created: 18 May 2021 1:49:46pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SynthSound.h"
#include "Data/AdsrData.h"
#include "Data/OscData.h"

class SynthVoice : public juce::SynthesiserVoice
{
public:
    bool canPlaySound (juce::SynthesiserSound* sound) override;
    void startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override;
    void stopNote (float velocity, bool allowTailOff) override;
    void pitchWheelMoved (int newPitchWheelValue) override;
    void controllerMoved (int controllerNumber, int newControllerValue) override;
    void prepareToPlay (double sampleRate, int samplesPerBlock, int outputChannels);
    void renderNextBlock (juce::AudioBuffer< float > &outputBuffer, int startSample, int numSamples) override;
    
    void updateADSR (const float attack, const float decay, const float sustain, const float release);
    OscData& getOscillator() { return osc; }
    
private:
    // juce::dsp::Oscillator<float> osc { [](float x) { return std::sin(x);}};
    OscData osc;
    juce::dsp::Gain<float> gain;
    AdsrData adsr;
    juce::AudioBuffer<float> synthBuffer; // created to fix clicking issue with oscillator
    
    bool isPrepared { false }; // Check that prepareToPlay has been run
};
