/*
  ==============================================================================

    OscData.h
    Created: 22 May 2021 4:22:47pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class OscData : public juce::dsp::Oscillator<float>
{
public:
    void prepareToPlay (juce::dsp::ProcessSpec& spec);
    void setWaveFrequency (const int midiNoteNumber);
    void setWaveType (const int choice);
    void getNextAudioBlock (juce::dsp::AudioBlock<float>& block);
    void setFmParams (const float depth, const float freq);
    
private:
    juce::dsp::Oscillator<float> fmOsc {[](float x) { return std::sin(x); }};
    float fmMod { 0.0f };
    float fmDepth { 0.0f };
    int lastMidiNote { 0 };
};
