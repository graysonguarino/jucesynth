/*
  ==============================================================================

    OscData.cpp
    Created: 22 May 2021 4:22:47pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "OscData.h"

void OscData::prepareToPlay (juce::dsp::ProcessSpec& spec)
{
    fmOsc.prepare (spec);
    prepare (spec);
}

void OscData::setWaveFrequency (const int midiNoteNumber)
{
    setFrequency (juce::MidiMessage::getMidiNoteInHertz(midiNoteNumber) + fmMod);
    lastMidiNote = midiNoteNumber;
}

void OscData::getNextAudioBlock (juce::dsp::AudioBlock<float>& block)
{
    for (int ch = 0; ch < block.getNumChannels(); ++ch)
    {
        for (int s = 0; s < block.getNumSamples(); ++s)
        {
            fmMod = fmOsc.processSample (block.getSample (ch, s)) * fmDepth; // Value of wave at point in time
        }
    }
    
    process (juce::dsp::ProcessContextReplacing<float> (block));
}

void OscData::setWaveType (const int choice)
{
    // NOTE: Oscillator can use a lookup table for greater effeciency.
    
    switch (choice)
    {
        case 0:
            // Sin wave
            initialise ([](float x) { return std::sin(x); });
            break;
            
        case 1:
            // Saw wave
            initialise ([](float x) { return x / juce::MathConstants<float>::pi; });
            break;
            
        case 2:
            // Square wave
            initialise ([](float x) { return x < 0.0f ? -1.0f : 1.0f; });
            break;
            
        default:
            jassertfalse;
            break;
    }
}

void OscData::setFmParams (const float depth, const float freq)
{
    fmOsc.setFrequency (freq);
    fmDepth = depth;
    setFrequency (juce::MidiMessage::getMidiNoteInHertz(lastMidiNote) + fmMod);
}
